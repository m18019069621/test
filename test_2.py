import os
import clip
import torch
import random
from network.HSPose import HSPose 
from tools.geom_utils import generate_RT
from config.config import *
from absl import app
import torch.nn.functional as F
from tools.eval_utils import load_depth, get_bbox

# FLAGS = flags.FLAGS
from data.dataset_3d import *
FLAGS = cfg_from_yaml_file('./config/config.yaml')
from evaluation.load_data_eval import PoseDataset
import numpy as np
import time

# from creating log
import tensorflow as tf
from evaluation.eval_utils import setup_logger
from evaluation.eval_utils_v1 import compute_degree_cm_mAP
from tqdm import tqdm

def seed_init_fn(seed):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    return

device = 'cuda'
clip_model, preprocess = clip.load(FLAGS.clip_model, device=device)
for param in clip_model.parameters():
    param.requires_grad = False
print('load clip model and freeze')


def evaluate(argv):
    if FLAGS.eval_seed == -1:
        seed = int(time.time())
    else:
        seed = FLAGS.eval_seed
    seed_init_fn(seed)
    if not os.path.exists(FLAGS.model_save):
        os.makedirs(FLAGS.model_save)
    # tf.compat.v1.disable_eager_execution()
    # logger = setup_logger('eval_log', os.path.join(FLAGS.model_save, 'log_eval.txt'))
    Train_stage = 'PoseNet_only'
    FLAGS.train = False

    # with open(FLAGS.model_save + '/config.yaml', 'w') as saved_config_file:
    #     yaml.dump(FLAGS, saved_config_file)

    model_name = os.path.basename(FLAGS.resume_model).split('.')[0]

    # get clip_text_features
    text_features = get_text_feat(clip_model)

    # build dataset annd dataloader
    print('FLAGS.dataset:',FLAGS.dataset)
    val_dataset = PoseDataset(source=FLAGS.dataset, mode='test',preprocess=preprocess)

    # output_path = os.path.join(FLAGS.model_save, f'eval_result_{model_name}')
    # if not os.path.exists(output_path):
    #     os.makedirs(output_path)
    # import pickle

    # t_inference = 0.0
    # img_count = 0
    # pred_result_save_path = os.path.join(output_path, 'pred_result.pkl')
    # # print(pred_result_save_path)
    # # print("*"*100)
    # if os.path.exists(pred_result_save_path):
    #     with open(pred_result_save_path, 'rb') as file:
    #         pred_results = pickle.load(file)
    #     img_count = 1
    # else:
    network = HSPose(Train_stage,clip_model)
    network = network.to(device)

    if FLAGS.resume:
        state_dict = torch.load(FLAGS.resume_model)['posenet_state_dict']
        unnecessary_nets = ['posenet.face_recon.conv1d_block', 'posenet.face_recon.recon_head']#, 'posenet.face_recon.face_head'
        for key in list(state_dict.keys()):
            for net_to_delete in unnecessary_nets:
                if key.startswith(net_to_delete):
                    state_dict.pop(key)
            # Adapt weight name to match old code version. 
            # Not necessary for weights trained using newest code. 
            # Dose not change any function. 
            if 'resconv' in key:
                state_dict[key.replace("resconv", "STE_layer")] = state_dict.pop(key)
        network.load_state_dict(state_dict, strict=True) 
    else:
        raise NotImplementedError
    
    
    # start to test
    network = network.eval()
    pred_results = []
    print('&'*200)
    # import time
    # time.sleep(60)
    # origin code
    for i, data in tqdm(enumerate(val_dataset, 1), dynamic_ncols=True):# i = index，且从1开始索引
        # print('data:', type(data),len(data),len(data[0]),len(data[1]),len(data[2]))
        
        if data is None:
            continue
        data, detection_dict, gts = data
        # detection:
        # ['image_path', 'gt_class_ids', 'gt_bboxes', 'gt_RTs', 'gt_scales', 'pred_class_ids', 'pred_bboxes', 'pred_scores', 'gt_handle_visibility']
        # print('image_path = ',detection_dict['image_path'])
        # print('gt_class_ids = ',detection_dict['gt_class_ids'])
        # print('pred_class_ids = ',detection_dict['pred_class_ids'])
        

        

        image_input = data['image_input_all'].to(device)
        cls_token,image_features_max = clip_model.encode_image(image_input)
        image_features = F.normalize(image_features_max, dim=-1, p=2)
        mean_shape = data['mean_shape'].to(device)
        sym = data['sym_info'].to(device)

        with torch.no_grad():
            output_dict \
            = network(
                        PC=data['pcl_in'].to(device), 
                        obj_id=data['cat_id_0base'].to(device), 
                        mean_shape=mean_shape,
                        sym=sym,
                        image_features=image_features.to(device),
                        text_features=text_features.to(device)
                        )
        max_indices = output_dict['max_indices'].to(device)
        
        if not torch.equal(data['cat_id_0base'].to(device), max_indices):
            i = 0
            img = Image.open('./data/NOCS/Real/'+ '/'.join(detection_dict['image_path'].split('/')[2:]) + '_color.png')
            save_imgpath = './check_class/'+ (detection_dict['image_path'].split('/')[-2])
            if not os.path.exists(save_imgpath):
                os.makedirs(save_imgpath)
            for bbox in detection_dict['pred_bboxes']:
                rmin, rmax, cmin, cmax = get_bbox(bbox)
                roi = img.crop((cmin, rmin, cmax, rmax))
                if data['cat_id_0base'][i].item() != max_indices[i].item():
                    roi.save(save_imgpath + '/' + detection_dict['image_path'].split('/')[-1] + str(data['cat_id_0base'][i].item()) + str(max_indices[i].item()) + '.png')
                i += 1
            print("save img done!")


            # save_imgpath = './check_class/'+ '/'.join(detection_dict['image_path'].split('/')[-2])
            # if not os.path.exists(save_imgpath):
            #     os.makedirs(save_imgpath)
            # print("not equal!!!"*10)
            # print('cat_id_0base = ',data['cat_id_0base'])
            # print('max_indices = ',max_indices)
            # img = Image.open('./data/NOCS/Real/'+ '/'.join(detection_dict['image_path'].split('/')[2:]) + '_color.png')
            # for bbox in detection_dict['pred_bboxes']:
            #     rmin, rmax, cmin, cmax = get_bbox(bbox)
            #     roi = img.crop((cmin, rmin, cmax, rmax))
            #     # print(save_imgpath)
            #     roi.save(save_imgpath + '/' + str(data['cat_id_0base'][i].item()) + str(max_indices[i].item()) + '.png')
            #     i += 1
            # print("save img done!")
        else:
            pass

        
                
                














def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)

def get_text_feat(clip_model):
    # NOCS cat_name and template
    with open(os.path.join('./data', 'templates.json')) as f:
            templates = json.load(f)[FLAGS.validate_dataset_prompt]
    cat_name2id = {'bottle': 1, 'bowl': 2, 'camera': 3, 'can': 4, 'laptop': 5, 'mug': 6}
    id2cat_name = {'1': 'bottle', '2': 'bowl', '3': 'camera', '4': 'can', '5': 'laptop', '6': 'mug'}
    # id2template = [] # 2 dim list   #id2template[0]:texts_of_bottle

    with torch.no_grad():
        text_features = []
        for cat_name in tqdm(id2cat_name.items()):
        # for cat_id, cat_name in id2cat_name.items():
            texts = [template.format(cat_name) for template in templates]
            texts = clip.tokenize(texts).to(device)
            if len(texts.shape) < 2:
                texts = texts[None, ...]
            class_embeddings = clip_model.encode_text(texts)# [64 512]
            class_embeddings /= class_embeddings.norm(dim=-1, keepdim=True)
            # class_embeddings_F = F.normalize(class_embeddings_origin, dim=-1, p=2) # same result
            class_embedding = class_embeddings.mean(dim=0)#[512]
            class_embedding /= class_embedding.norm()#[512]
            text_features.append(class_embedding)
        text_features = torch.stack(text_features, dim = 0)# [6, 512]
    return text_features

if __name__ == "__main__":
    app.run(evaluate)