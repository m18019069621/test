import time
import torch
import clip
from PIL import Image
import os
from data.dataset_3d import *
FLAGS = cfg_from_yaml_file('./config/config.yaml')
from evaluation.load_data_eval import PoseDataset as PoseDataset_eval
from datasets.load_data import PoseDataset as PoseDataset_train
from tqdm import tqdm
import _pickle as cPickle
import cv2
import matplotlib.pyplot as plt
from tools.eval_utils import load_depth, get_bbox
import torch.nn.functional as F
import torch.nn as nn
# print(clip.available_models())
# model_dir = './clip/clip_model/ViT-B-16.pt'
# # print(os.path.isfile(model_dir))
# model, preprocess = clip.load(model_dir, device='cuda')



# print('FLAGS.dataset:',FLAGS.dataset)
# val_dataset = PoseDataset_eval(source=FLAGS.dataset, mode='test')

# for i, data in tqdm(enumerate(val_dataset, 1), dynamic_ncols=True):# i = index，且从1开始索引
#     # print('data:', type(data),len(data),len(data[0]),len(data[1]),len(data[2]))
#     if data is None:
#         continue
#     data, detection_dict, gts = data
#     # print(data.keys())
def _depth_to_pcl(self, depth, K, xymap, mask):
    K = K.reshape(-1)
    cx, cy, fx, fy = K[2], K[5], K[0], K[4]
    depth = depth.reshape(-1).astype(np.float)
    valid = ((depth > 0) * mask.reshape(-1)) > 0
    depth = depth[valid]
    x_map = xymap[0].reshape(-1)[valid]
    y_map = xymap[1].reshape(-1)[valid]
    real_x = (x_map - cx) * depth / fx
    real_y = (y_map - cy) * depth / fy
    pcl = np.stack((real_x, real_y, depth), axis=-1)
    return pcl.astype(np.float32)

def _sample_points(self, pcl, n_pts):
    """ Down sample the point cloud using farthest point sampling.

    Args:
        pcl (torch tensor or numpy array):  NumPoints x 3
        num (int): target point number
    """
    total_pts_num = pcl.shape[0]
    if total_pts_num < n_pts:
        pcl = np.concatenate([np.tile(pcl, (n_pts // total_pts_num, 1)), pcl[:n_pts % total_pts_num]], axis=0)
    elif total_pts_num > n_pts:
        ids = np.random.permutation(total_pts_num)[:n_pts]
        pcl = pcl[ids]
    return pcl

def trim_zeros(x):
    '''It's common to have tensors larger than the available data and
    pad with zeros. This function removes rows that are all zeros.
    x: [rows, columns].
    '''

    pre_shape = x.shape
    assert len(x.shape) == 2, x.shape
    new_x = x[~np.all(x == 0, axis=1)]
    post_shape = new_x.shape
    assert pre_shape[0] == post_shape[0]
    assert pre_shape[1] == post_shape[1]

    return new_x

def crop_resize_by_warp_affine(img, center, scale, output_size, rot=0, interpolation=cv2.INTER_LINEAR):
    '''
    output_size: int or (w, h)
    NOTE: if img is (h,w,1), the output will be (h,w)
    '''
    if isinstance(scale, (int, float)):
        scale = (scale, scale)
    if isinstance(output_size, int):
        output_size = (output_size, output_size)
    trans = get_affine_transform(center, scale, rot, output_size)

    dst_img = cv2.warpAffine(img, trans, (int(output_size[0]), int(output_size[1])), flags=interpolation)

    return dst_img

def get_affine_transform(center, scale, rot, output_size, shift=np.array([0, 0], dtype=np.float32), inv=False):
    '''
    adapted from CenterNet: https://github.com/xingyizhou/CenterNet/blob/master/src/lib/utils/image.py
    center: ndarray: (cx, cy)
    scale: (w, h)
    rot: angle in deg
    output_size: int or (w, h)
    '''
    if isinstance(center, (tuple, list)):
        center = np.array(center, dtype=np.float32)

    if isinstance(scale, (int, float)):
        scale = np.array([scale, scale], dtype=np.float32)

    if isinstance(output_size, (int, float)):
        output_size = (output_size, output_size)

    scale_tmp = scale
    src_w = scale_tmp[0]
    dst_w = output_size[0]
    dst_h = output_size[1]

    rot_rad = np.pi * rot / 180
    src_dir = get_dir([0, src_w * -0.5], rot_rad)
    dst_dir = np.array([0, dst_w * -0.5], np.float32)

    src = np.zeros((3, 2), dtype=np.float32)
    dst = np.zeros((3, 2), dtype=np.float32)
    src[0, :] = center + scale_tmp * shift
    src[1, :] = center + src_dir + scale_tmp * shift
    dst[0, :] = [dst_w * 0.5, dst_h * 0.5]
    dst[1, :] = np.array([dst_w * 0.5, dst_h * 0.5], np.float32) + dst_dir

    src[2:, :] = get_3rd_point(src[0, :], src[1, :])
    dst[2:, :] = get_3rd_point(dst[0, :], dst[1, :])

    if inv:
        trans = cv2.getAffineTransform(np.float32(dst), np.float32(src))
    else:
        trans = cv2.getAffineTransform(np.float32(src), np.float32(dst))

    return trans

def get_dir(src_point, rot_rad):
    sn, cs = np.sin(rot_rad), np.cos(rot_rad)

    src_result = [0, 0]
    src_result[0] = src_point[0] * cs - src_point[1] * sn
    src_result[1] = src_point[0] * sn + src_point[1] * cs

    return src_result

def get_3rd_point(a, b):
    direct = a - b
    return b + np.array([-direct[1], direct[0]], dtype=np.float32)
def get_2d_coord_np(width, height, low=0, high=1, fmt='CHW'):
    '''
    Args:
        width:
        height:
    Returns:
        xy: (2, height, width)
    '''
    # coords values are in [low, high]  [0,1] or [-1,1]
    x = np.linspace(0, width-1, width, dtype=np.float32)
    y = np.linspace(0, height-1, height, dtype=np.float32)
    xy = np.asarray(np.meshgrid(x, y))
    if fmt == 'HWC':
        xy = xy.transpose(1, 2, 0)
    elif fmt == 'CHW':
        pass
    else:
        raise ValueError(f'Unknown format: {fmt}')
    return xy

# with open('./data/NOCS/Real/test/scene_2/0000' + '_label.pkl', 'rb') as f:
#     gts = cPickle.load(f)
# print(len(gts))
# print('gts = ',gts.keys())
# print('class_ids =',gts['class_ids'])
# print('handle_visibility =',gts['handle_visibility'])

# with open('./data/NOCS/Real/test/scene_2/0629' + '_label.pkl', 'rb') as f2:
#     gts2 = cPickle.load(f2)

# print('class_ids =',gts2['class_ids'])
# print('size =',gts2['size'])
# print('scales =',gts2['scales'])

# device = 'cuda'
# path = './data/NOCS/Real/test/scene_5/'
# result_path = './data/results/nocs_results/real_test/results_test_scene_5_'
# model, preprocess = clip.load(FLAGS.clip_model, device=device)
# error_img = []
# for i in range(1,517):
#     num = f'{i:04d}'
#     pkl_path = result_path + num + '.pkl'
#     # print('pkl_path = ',pkl_path)
#     img_path1 = path + num + '_color.png'
#     try:
#         with open(pkl_path , 'rb') as f:
#             detctions = cPickle.load(f)
#     except FileNotFoundError as e:
#         print('KeyError: ', e)
#         continue
#     print('detctions-gt_class_ids = ',detctions['gt_class_ids'])
#     print('detctions-pred_class_ids = ',detctions['pred_class_ids'])
#     print('*'*20)

#     try:
#         rgb = cv2.imread(img_path1)
#     except FileNotFoundError as e:
#         print('KeyError: ', e)
#         continue
#     bbox = detctions['pred_bboxes']
#     clip_pre = []
    
#     for box in bbox:
#         rmin, rmax, cmin, cmax = get_bbox(box)
#         bbox_xyxy = np.array([cmin, rmin, cmax, rmax])
#         x1, y1, x2, y2 = bbox_xyxy
#         roi = rgb[y1:y2, x1:x2]
#         desired_size = (224, 224)
#         resized_roi = cv2.resize(roi, desired_size)
#         resized_roi = Image.fromarray(cv2.cvtColor(resized_roi, cv2.COLOR_BGR2RGB))
#         image = preprocess(resized_roi).unsqueeze(0).to(device)
#         text = clip.tokenize(['a bottle', 'a bowl', 'a camera', 'a can', 'a laptop', 'a mug']).to(device)
#         with torch.no_grad():
#             image_features = model.encode_image(image)
#             text_features = model.encode_text(text)
            
#             logits_per_image, logits_per_text = model(image, text)
#             probs = logits_per_image.softmax(dim=-1).cpu().numpy()
#         max_index = np.argmax(probs)
#         # print('max_index:', max_index+1)
#         clip_pre.append(max_index+1)

#     clip_pre = np.array(clip_pre)
#     print('clip_pre = ',clip_pre)
#     are_equal = np.array_equal(detctions['pred_class_ids'], (clip_pre))
#     if not are_equal:
#         print('二者的元素和顺序不一致')
#         print(img_path1)
#         error_img.append(img_path1)
# print('error_img',error_img)


device = 'cuda'
clip_model, preprocess = clip.load(FLAGS.clip_model, device=device)
print(preprocess)
logit_scale = nn.Parameter(torch.ones([]) * np.log(1 / 0.07)).to(device)
print('logit_scale = ',logit_scale)
logit_scale = logit_scale.exp()

# with open('./data/results/nocs_results/real_test/results_test_scene_5_0019.pkl' , 'rb') as f:
#     detctions = cPickle.load(f)
# print('detctions = ',len(detctions),detctions.keys())
# print('detctions-image_path = ',detctions['image_path'])
# print('detctions-gt_class_ids = ',detctions['gt_class_ids'])
# print('detctions-pred_class_ids = ',detctions['pred_class_ids'])
# num_instance = len(detctions['pred_class_ids'])
# print('num_instance = ',num_instance)
# print('detctions-gt_bboxes = ',detctions['gt_bboxes'])
# print('detctions-gt_RTs = ',detctions['gt_RTs'].shape)
# print('detctions-gt_scales = ',detctions['gt_scales'])
# print('detctions-pred_bboxes = ',detctions['pred_bboxes'])
# print('detctions-pred_scores = ',detctions['pred_scores'],type(detctions['pred_scores']))
# print('detctions-pred_masks = ',detctions['pred_masks'].shape)
# print('detctions-gt_handle_visibility = ',detctions['gt_handle_visibility'])
# print('*'*10)
img_path1 = './data/NOCS/Real/train/scene_7/0001' 
idx1 = 0 # laptop
img_path2 = './data/NOCS/Real/train/scene_4/0000' 
idx2 = 0 # mug
img_path3 = './data/NOCS/Real/train/scene_3/0000' 
idx3 = 0 # bowl
img_path4 = './data/NOCS/Real/train/scene_6/0000' 
idx4 = 0 # camera
img_path5 = './data/NOCS/Real/train/scene_5/0000' 
idx5 = 2 # can


image_paths = [img_path1, img_path2, img_path3, img_path4, img_path5]
idxs = [0,0,0,0,2]
# laptop mug bowl camera can
gts_dict = {}
i = 0
for img_path in image_paths:
    with open(img_path + '_label.pkl' , 'rb') as f:
        gts = cPickle.load(f)
    gts_dict[i] = gts
    print('gts = ',len(gts),gts.keys())
    print('gts-class_ids = ',gts['class_ids'])
    print('gts-bboxes = ',gts['bboxes'],type(gts['bboxes']))
    print('gts-scales = ',gts['scales'])
    # print('gts-instance_ids = ',gts['instance_ids'])
    print('gts-model_list = ',gts['model_list'])
    print('*'*20)
    i += 1
# print('gts_dict = ',gts_dict)


################### image_feat[5 1 512]
with torch.no_grad():
    image_feat = []
    gt_clss = []
    image_input_bs = []
    j = 0
    for img_path in image_paths:
        img = Image.open(img_path + '_color.png')
        rmin, rmax, cmin, cmax = get_bbox(gts_dict[j]['bboxes'][idxs[j]])
        gt_cls = gts_dict[j]['class_ids'][idxs[j]] - 1 #transfer to 0based
        roi = img.crop((cmin, rmin, cmax, rmax))
        # roi_resized = roi.resize((224, 224))
        image_input = preprocess(roi).unsqueeze(0).to(device)
        image_input_bs.append(image_input)
        # print('image_input = ',(image_input.shape))
        image_features,_ = clip_model.encode_image(image_input)
        image_features = F.normalize(image_features, dim=-1, p=2)
        print('image_features = ',(image_features.shape),type(image_features))
        image_feat.append(image_features)
        gt_clss.append(gt_cls)
        j += 1
    image_feat_bs = torch.cat([image_feat[0], image_feat[1], image_feat[2], image_feat[3], image_feat[4]], dim=0).to(device)
    image_input_bs = torch.cat([image_input_bs[0], image_input_bs[1], image_input_bs[2], image_input_bs[3], image_input_bs[4]], dim=0).to(device)
    # gt_clss_bs = torch.tensor(gt_clss[:4]).to(device)
    gt_clss_bs = torch.tensor(gt_clss).to(device)
print('image_feat',image_feat_bs.shape)
print('image_input_bs',image_input_bs.shape)
img_feat_test,img_feat_all = clip_model.encode_image(image_input_bs)
img_feat_test = F.normalize(img_feat_test, dim=-1, p=2) # 人工设计的cls_token（含有类别信息）
print('img_feat_test = ',(img_feat_test.shape))
img_feat_all = F.normalize(img_feat_all, dim=-1, p=2) # vit提取的类别最大池化后的特征（含有语义信息）
# img_feat_all = img_feat_all.max(1)[0]
print('img_feat_all = ',(img_feat_all.shape))


# expanded_img_features = image_feat_bs.unsqueeze(1).repeat(1, 1024, 1)
# print('expanded_img_features',expanded_img_features.shape)
# print('gt_clss_bs',gt_clss_bs)# [1 5]
# print('gt_clss_bs.t()',gt_clss_bs.view(-1, 1).shape)# [5 1]



################### texts_feature[6 512]
# NOCS cat_name and template
with open(os.path.join('./data', 'templates.json')) as f:
        templates = json.load(f)[FLAGS.validate_dataset_prompt]
cat_name2id = {'bottle': 1, 'bowl': 2, 'camera': 3, 'can': 4, 'laptop': 5, 'mug': 6}
id2cat_name = {'1': 'bottle', '2': 'bowl', '3': 'camera', '4': 'can', '5': 'laptop', '6': 'mug'}
# id2template = [] # 2 dim list   #id2template[0]:texts_of_bottle

text_features = []
# for cat_name in tqdm(id2cat_name.items()):
with torch.no_grad():
    for cat_id, cat_name in id2cat_name.items():
        texts = [template.format(cat_name) for template in templates]
        # id2template.append(texts)
        # print('texts = ',texts)
        texts = clip.tokenize(texts).to('cuda')
        if len(texts.shape) < 2:
            texts = texts[None, ...]
        class_embeddings = clip_model.encode_text(texts)# [64 512]
        class_embeddings /= class_embeddings.norm(dim=-1, keepdim=True)
        # class_embeddings_F = F.normalize(class_embeddings_origin, dim=-1, p=2) # same result
        class_embedding = class_embeddings.mean(dim=0)#[512]
        class_embedding /= class_embedding.norm()#[512]
        text_features.append(class_embedding)
    text_features = torch.stack(text_features, dim = 0)# [6, 512]
print('text_features = ',text_features.shape)
print('=> encoding templates done')

mlp_pc_img = nn.Sequential(
                nn.Linear(1024, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(inplace=True),
                nn.Linear(1024, 512),
            ).to('cuda')

################### pc_feat[5 1 512]
pc_feat = torch.load('./pc_feat/fm4tensor([4, 5, 1, 2, 3]).pth').to(torch.float16)
# pc_feat = pc_feat.repeat(1, 1024, 1)
print('pc_feat = ',pc_feat.shape)
# concatenated_tensor = torch.cat([pc_feat, expanded_img_features], dim=2).float()
# print('concatenated_tensor = ',concatenated_tensor.shape)
pc_feat = F.normalize(pc_feat.squeeze(dim=1), dim=-1, p=2)# 0based

print('pc_feat = ',pc_feat.shape)
# output = mlp_pc_img(concatenated_tensor)
# print('output = ',output.shape)


EntropyLoss = nn.CrossEntropyLoss()
# cat_one-hot
one_hot_matrix = torch.eye(6, device=device)[gt_clss_bs] # [5 6]
print('one_hot_matrix = ',one_hot_matrix,one_hot_matrix.shape)

# pc-img_gt
# max_index = torch.max(gt_clss_bs)
# gt_pc_img = torch.zeros(gt_clss_bs.size(0), gt_clss_bs.size(0))
# gt_clss_bs = torch.tensor([4,5,5,3,6])
expanded_tensor = gt_clss_bs.unsqueeze(0)
gt_pc_img = (expanded_tensor == expanded_tensor.t()).float()

gt_pc_img = 0.8 * gt_pc_img  #+ torch.eye(gt_clss_bs.size(0)).to(device)
gt_pc_img.fill_diagonal_(1)
print('gt_pc_img',gt_pc_img,gt_pc_img.shape)


tensor1 = torch.tensor([5, 4, 1, 2, 3])
tensor2 = torch.tensor([5, 4, 1, 2, 3])

# 使用 torch.equal() 检查两个张量是否完全相同
if torch.equal(tensor1, tensor2):
    print("两个张量完全相同。")
else:
    print("两个张量不完全相同。")

# with torch.no_grad():
#     print('logit_scale = ',logit_scale)
#     logits_img_text = (100 * image_feat_bs @ text_features.T).softmax(dim=-1)# [5 6]
#     logits_text_img = (100 * text_features @ image_feat_bs.T).softmax(dim=-1)# [6 5]
#     print('logits_img_text = ',logits_img_text)
#     print('logits_text_img.T = ',logits_text_img.T)
#     loss1 = (F.cross_entropy(logits_img_text, gt_clss_bs) + F.cross_entropy(logits_text_img.T, gt_clss_bs)) / 2
#     print('loss1 = ',loss1,type(loss1))


#     logits_pc_text = (100 * pc_feat @ text_features.T).softmax(dim=-1)# [5 6]
#     logits_text_pc = (100 * text_features @ pc_feat.T).softmax(dim=-1)# [6 5]
#     print('logits_pc_text = ',logits_pc_text)
#     print('logits_text_pc.T = ',logits_text_pc.T)
#     loss2 = (F.cross_entropy(logits_pc_text, gt_clss_bs) + F.cross_entropy(logits_text_pc.T, gt_clss_bs)) / 2
#     print('loss2 = ',loss2,type(loss2))


#     logits_pc_img = (100 * pc_feat @ image_feat_bs.T).softmax(dim=-1)# [5 5]
#     logits_img_pc = (100 * image_feat_bs @ pc_feat.T).softmax(dim=-1)# [5 5]
#     print('logits_pc_img = ',logits_pc_img)
#     print('logits_img_pc.T = ',logits_img_pc.T)
#     loss3 = (F.cross_entropy(logits_pc_img, gt_pc_img) + F.cross_entropy(logits_img_pc.T, gt_pc_img)) / 2
#     print('loss3 = ',loss3,type(loss3))

#     # loss = (F.cross_entropy(logits_img_text, one_hot_matrix) + F.cross_entropy(logits_text_img.T, one_hot_matrix)) / 2
    
#     pred = torch.argmax(logits_img_text, dim=-1)
#     correct = pred.eq(gt_clss_bs).sum()
#     print('pred correct = ',pred,correct)


# test = torch.tensor([4,5,5,3,3])
# max_index = torch.max(test)
# gt_pc_img = torch.zeros(max_index + 1, max_index + 1)
# expanded_tensor = test.unsqueeze(0)
# similarity_matrix = (expanded_tensor == expanded_tensor.t()).float()
# print('similarity_matrix',similarity_matrix,similarity_matrix.shape)
# # cat转换为onehot
# num_classes = 6
# cat_id = gts['class_ids']
# cat_id = torch.as_tensor(cat_id, dtype=torch.float32).contiguous()
# print('cat_id.shape[0] = ',type(cat_id),cat_id.shape[0])
# one_hot = torch.zeros(cat_id.size(0), num_classes)
# obj_idh = cat_id.view(-1, 1)
# one_hot = one_hot.scatter_(1, obj_idh.long() - 1, 1) # 索引要从0开始，在load_data中已经转换为0_based了
# print('initial_one_hot = ',one_hot)

# for bbox in gts['bboxes']:
#     # bbox = gts['pred_bboxes'][0]
#     test_pc = torch.rand(1, 512, dtype=torch.float16).to(device)
#     # print('test_pc = ',test_pc)

#     rmin, rmax, cmin, cmax = get_bbox(bbox)
#     print('rmin, rmax, cmin, cmax = ',rmin, rmax, cmin, cmax)
#     bbox_xyxy = np.array([cmin, rmin, cmax, rmax])
#     print('bbox_xyxy = ',bbox_xyxy)
#     # x1, y1, x2, y2 = bbox_xyxy
#     # print('x1, y1, x2, y2 = ',x1, y1, x2, y2)

#     roi = img.crop((cmin, rmin, cmax, rmax))
#     # desired_size = (224, 224)
#     roi_resized = roi.resize((224, 224))
#     # print('resized_roi_shape= ',roi_resized.shape)
#     # if roi_resized is not None:
#     #     roi_resized.show()
#     # else:
#     #     print('Failed to load the image.')
#     # resized_roi = Image.fromarray(cv2.cvtColor(resized_roi, cv2.COLOR_BGR2RGB))
#     # print('type(resized_roi) = ',type(resized_roi))
#     image = preprocess(roi_resized).unsqueeze(0).to(device)
#     # text = clip.tokenize(['a bottle', 'a bowl', 'a camera', 'a can', 'a laptop', 'a mug']).to(device)

#     image_features = clip_model.encode_image(image)
#     print('image_features = ',image_features.shape,type(image_features))
#     # text_features = clip_model.encode_text(text)
    

#     # text_features = F.normalize(text_features, dim=-1, p=2)
#     image_features = F.normalize(image_features, dim=-1, p=2)
#     test_pc = F.normalize(test_pc, dim=-1, p=2)
#     print('img text pc = ',image_features.shape,text_features.shape,test_pc.shape)


    
#     # logits_text_img = logit_scale * image_features @ text_features.t()
#     logits_text_img =(100.0 * image_features @ text_features.T).softmax(dim=-1)
#     print('logits_text_img = ',logits_text_img.shape,logits_text_img)


    # max_index = np.argmax(logits_text_img)
    # print('max_index:', max_index+1) 


    # with torch.no_grad():
    #     image_features = clip_model.encode_image(image)
    #     text_features = clip_model.encode_text(text)

    #     print(image_features.shape,text_features.shape)
        
    #     logits_per_image, logits_per_text = clip_model(image, text)
    #     probs = logits_per_image.softmax(dim=-1).cpu().numpy()

    # print('Label probs:', probs) 
    # max_index = np.argmax(probs)
    # print('max_index:', max_index+1) 


# coord_2d = get_2d_coord_np(im_W, im_H).transpose(1, 2, 0)
# print('coord_2d',coord_2d.shape)


# rmin, rmax, cmin, cmax = get_bbox(bbox)
# bbox_xyxy = np.array([cmin, rmin, cmax, rmax])

# x1, y1, x2, y2 = bbox_xyxy
# roi = rgb[y1:y2, x1:x2]
# desired_size = (224, 224)
# resized_roi = cv2.resize(roi, desired_size)
# print('resized_roi',resized_roi.shape)

# if resized_roi is not None:
#     cv2.imshow('resized_roi', resized_roi)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()
# else:
#     print('Failed to load the image.')
# start = time.time()
# resized_roi = Image.fromarray(cv2.cvtColor(resized_roi, cv2.COLOR_BGR2RGB))
# end = time.time()
# print(end-start)
# resized_roi.show()
# model, preprocess = clip.load(FLAGS.clip_model, device=device)
# image = preprocess(resized_roi).unsqueeze(0).to(device)
# text = clip.tokenize(['a bottle', 'a bowl', 'a camera', 'a can', 'a laptop', 'a mug']).to(device)
# with torch.no_grad():
#     image_features = model.encode_image(image)
#     text_features = model.encode_text(text)

#     print(image_features.shape,text_features.shape)
    
#     logits_per_image, logits_per_text = model(image, text)
#     probs = logits_per_image.softmax(dim=-1).cpu().numpy()

# print('Label probs:', probs) 
# max_index = np.argmax(probs)
# print('max_index:', max_index+1) 

# cat_id = torch.tensor([[1], [2], [3], [4]])
# obj_idh = cat_id.view(-1, 1)
# one_hot = torch.zeros(4, FLAGS.obj_c).to(cat_id.device).scatter_(1, obj_idh.long(), 1)
# one_hot = one_hot.unsqueeze(1).repeat(1, 1024, 1) 
# print('one_hot = ',one_hot.shape)


# here resize and crop to a fixed size 256 x 256
# cx = 0.5 * (x1 + x2)
# cy = 0.5 * (y1 + y2)
# bbox_center = np.array([cx, cy])  # (w/2, h/2)
# scale = max(y2 - y1, x2 - x1)
# scale = min(scale, max(im_H, im_W)) * 1.0


# roi_coord_2d = crop_resize_by_warp_affine(
#                 coord_2d, bbox_center, scale, 224, interpolation=cv2.INTER_NEAREST
#             ).transpose(2, 0, 1)
# print('roi_coord_2d',roi_coord_2d.shape)



# for j in range(num_instance):
#     cat_id = detctions['pred_class_ids'][j]
#     print('cat_id = ',cat_id)
#     mask = detctions['pred_masks'][:, :, j]
#     bbox = detctions['pred_bboxes'][j]
#     print('bbox',bbox)
#     rmin, rmax, cmin, cmax = get_bbox(bbox)
#     print('get_bbox',rmin, rmax, cmin, cmax)
#     bbox_xyxy = np.array([cmin, rmin, cmax, rmax])
#     x1, y1, x2, y2 = bbox_xyxy
#     print('bbox_xyxy',x1, y1, x2, y2)
#     cx = 0.5 * (x1 + x2)
#     cy = 0.5 * (y1 + y2)
#     bbox_center = np.array([cx, cy])  # (w/2, h/2)
#     scale = max(y2 - y1, x2 - x1)
#     scale = min(scale, max(im_H, im_W)) * 1.0
#     print('scale',scale)

